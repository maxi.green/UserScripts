// ==UserScript==
// @author   Maxi Green
// @name     Only show GIF animations while the mouse hovers over the image
// @version  1
// @include  *
// ==/UserScript==

// Regular expression for GIF image URLs
const gif_re = /.*\.gif/i;

if (window.top===window) {

  const images = document.getElementsByTagName("img");
  for (i = 0; i < images.length; i++) {
    const img = images[i];

    function img_onload() {
      // Only execute this handler once (after the image has been loaded the first time)
      // Otherwise, this would be executed whenever the image is changed.
      img.removeEventListener("load", img_onload);

      const src_orig = img.src;
      const srcset_orig = img.srcset;

      // Draw (a static version of) the image onto a canvas
      const canvas = document.createElement("canvas");
      canvas.width = img.width;
      canvas.height = img.height;
      canvas.getContext("2d").drawImage(img, 0, 0, img.width, img.height);
      const src_static = canvas.toDataURL("image/png");

      // Replace the image and create mouse hover handlers
      img.src = src_static;
      img.srcset = "";
      img.classList.add("frozen_gif");

      // Add event handlers that replace the image with either the original URL
      // or the static image
      img.addEventListener("mouseenter", function () {
        // Show the original image
        img.src = src_orig;
        img.srcset = srcset_orig;
        img.classList.remove("frozen_gif");

      });
      img.addEventListener("mouseleave", function (){
        // Show the frozen image
        img.src = src_static;
        img.srcset = "";
        img.classList.add("frozen_gif");
      });
    }

    // Check weather the image is a GIF
    if (gif_re.test(img.src)) {
      // Wait for the image to be loaded and execute the above function
      img.addEventListener("load", img_onload);
    }
  }

  // Add a style class for frozen GIFs
  let styleSheet = document.createElement("style");
  styleSheet.type = "text/css";
  styleSheet.innerText =
    'img.frozen_gif { border: 2px dashed blue !important; margin: -2px important; }';
  document.head.appendChild(styleSheet);
}
